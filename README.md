
# Github API Frontend
## Synopsis
Github-API-Frontend is a working demo app to showcase skills relating to containerized REST API architecture and implementation.

Live demo available [here](https://gaf.lbgarber.com/)

## Functionality
- User registration and authorization through JWT
- Administration through administrator users and administrator-exclusive APIs and pages
- Event logging/User tracking
- Searching Github repositories 
- Favoriting repositories and accessing favorited repositories

## Essential Technologies
- Golang
- JWT (JSON Web Token)
- MongoDB
- Redis
- Docker/docker-compose
- Vue.js
- Github API

## Services
- [API](https://gitlab.com/github-api-frontend/api)
- [Web](https://gitlab.com/github-api-frontend/web)

## Arthitecture Diagram
![Architecture Diagram](images/flow_diagram.png)
